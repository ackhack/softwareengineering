package FileCopy;

import java.io.File;

public class Configurator {

    private static Configurator configurator;
    private static FileInterface filecopyLogic;
    private static NotificationInterface notification;
    private static File input;
    private static File output;

    private Configurator() {}

    public void initialize() {
        filecopyLogic = new FilecopyLogic();
        notification = new Notification();
    }

    public void sendMessage(String message) {
        notification.sendMessage(message);
    }

    public static Configurator getInstance() {
        if (configurator == null) {
            configurator = new Configurator();
            configurator.initialize();
        }
        return configurator;
    }

    public FileInterface getFilecopyLogic() {
        return filecopyLogic;
    }

    public File getInputFile() {
        return input;
    }

    public File getOutputFile() {
        return output;
    }

    public void readFiles(String... paths) {
        try {
            input = new File(paths[0]);
            output = new File(paths[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
