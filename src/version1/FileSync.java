package version1;

import java.io.*;

public class FileSync {

    public static void main(String[] args) {

        try {

            File file = new File("LogFile.log");   //Log-Datei erstellen
            PrintStream ps = new PrintStream(file);

            try {

                while (true) {

                    if (args.length != 2) { //Testen ob es 2 Argumente sind
                        throw new Exception("Unvalid Arguments: Please use 2 Paths");
                    }

                    File inputFile = new File(args[0]); //Dateien einlesen
                    File outputFile = new File(args[1]);

                    if (inputFile.length() == outputFile.length()) {    //Wenn Dateien gleich groß sind starte Analyse

                        if (!FileComparator.compare(inputFile, outputFile)) {    //Dateien vergleichen
                            FileWriter.write(inputFile, outputFile);    //Datei überschreiben
                        }

                    } else {
                        FileWriter.write(inputFile, outputFile);    // Datei überschreiben
                    }

                    Thread.sleep(1000); //Kurze Pause damit System nicht überladen wird
                }

            } catch (Exception ex) {    //Fehlerbehandlung durch Rückgabe an Log-Datei
                ex.printStackTrace(ps);
            }
            ps.close();

        } catch (FileNotFoundException nofile) {
            nofile.printStackTrace(); //Log-Datei kann nicht geschrieben werden
        }
    }
}