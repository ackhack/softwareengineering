package FileCopy;

import java.io.File;

public interface FileInterface {

    void copyFile(File source, File target);
}
