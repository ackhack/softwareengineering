package FileCopy;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class ConfiguratorTest {

    private static Configurator configurator;

    @BeforeClass
    public static void setup() {
        configurator = Configurator.getInstance();
    }

    @Test
    public void test() throws IOException {

        String[] array = new String[] {"src/FileCopy/input","src/FileCopy/output"};
        ConsoleUI.main(array);

        File in = configurator.getInputFile();
        File out = configurator.getOutputFile();
        String inString = Files.readString(Paths.get(in.getAbsolutePath()), StandardCharsets.UTF_8);
        String outString = Files.readString(Paths.get(out.getAbsolutePath()), StandardCharsets.UTF_8);

        assertEquals(inString, outString);
    }
}


