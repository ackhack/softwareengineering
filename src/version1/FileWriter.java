package version1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

class FileWriter {

    static void write(File inputFile, File outputFile) throws Exception {

        FileInputStream fIn = null; //Streams initialisieren
        FileOutputStream fOut = null;

        try {

            fIn = new FileInputStream(inputFile);   //Dateien einlesen
            fOut = new FileOutputStream(outputFile);

            int offset = 0; //offset für Buffer

            while (offset < inputFile.length()) {   //4096 Byte blockweise übertragen

                byte[] content = new byte[4096]; //ByteBuffer
                int length = fIn.read(content, offset, 4096);   //Bytes lesen

                fOut.write(content, offset, length);    //Bytes schreiben
                offset += 4096;
            }


        } finally { //Streams schließen
            try {
                fIn.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            try {
                fOut.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}