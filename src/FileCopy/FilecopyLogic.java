package FileCopy;

import java.io.*;

public class FilecopyLogic implements FileInterface {

    public void copyFile(File source, File target) {

        Configurator configurator = Configurator.getInstance();
        FileInputStream fIn = null; //Streams initialisieren
        FileOutputStream fOut = null;

        try {

            fIn = new FileInputStream(source);   //Dateien einlesen
            fOut = new FileOutputStream(target);

            int offset = 0; //offset für Buffer

            while (offset < source.length()) {   //4096 Byte blockweise übertragen

                byte[] content = new byte[4096]; //ByteBuffer
                int length = fIn.read(content, offset, 4096);   //Bytes lesen

                fOut.write(content, offset, length);    //Bytes schreiben
                offset += 4096;
            }

            configurator.sendMessage("Successfully copied File");

        } catch (Exception e) {

            configurator.sendMessage("An Error occurred during Filecopy");
            e.printStackTrace();

        } finally { //Streams schließen
            try {
                assert fIn != null;
                fIn.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            try {
                assert fOut != null;
                fOut.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }
}
