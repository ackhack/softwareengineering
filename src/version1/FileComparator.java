package version1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

class FileComparator {

    static boolean compare(File inputFile, File outputFile) throws Exception {

        BufferedReader inputReader = null;  //Reader initialisieren
        BufferedReader outputReader = null;

        try {

            inputReader = new BufferedReader(new FileReader(inputFile)); //Dateien Zeilen für Zeile vergleichen
            outputReader = new BufferedReader(new FileReader(outputFile));

            String inputString = inputReader.readLine();    //Strings initialisieren
            String outputString = outputReader.readLine();

            while (inputString != null && outputString != null) { //Schleife läuft bis beide Dateien am Ende sind

                if (!outputString.equals(inputString)) {    //Wenn Zeilen ungleich sind gehe zu überschreiben
                    break;  //Schleife abbrechen wenn eine Zeile ungleich ist
                }

                inputString = inputReader.readLine();   //nächste Zeile
                outputString = outputReader.readLine();
            }

            return inputString == null && outputString == null; //true oder false zurückgeben

        } finally { //Streams schließen
            try {
                inputReader.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            try {
                outputReader.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}