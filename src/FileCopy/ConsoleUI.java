package FileCopy;

public class ConsoleUI {
    public static void main(String[] args) {

        if (args.length != 2) {    //Checks for Errors in Arguments
            System.out.println("Error: Unusable Paths, please enter two Paths");
        }

        Configurator configurator = Configurator.getInstance(); //Get Configurator-Singleton
        configurator.readFiles(args);   //Read File-Location

        configurator.getFilecopyLogic().copyFile(   //Copy File
                configurator.getInputFile(),
                configurator.getOutputFile());
    }
}
